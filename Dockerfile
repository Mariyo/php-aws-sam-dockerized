FROM webdevops/php-dev:7.3

RUN apt-get update
RUN apt-get install -y python python-pip sudo apache2-utils apt-utils

ENV PATH="~/.local/bin:${PATH}"

USER "application"

RUN pip install awscli --no-cache-dir --upgrade
RUN pip install aws-sam-cli --user