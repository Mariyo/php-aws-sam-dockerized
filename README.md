# Dockerized PHP + AWS for Serverless Workshop

[Documentation](https://dockerfile.readthedocs.io/en/latest/content/DockerImages/dockerfiles/php-dev.html)

## Requirements

- docker (min 17.03.1-ce)
- docker-compose (min 1.13.0)

## Installation

```bash
./ops up
# or
./ops up -d
# or
./ops up -d --build
```

## Shortcuts

```bash
./ops bash
./ops php "index.php"
./ops bash -c "composer --version"
./ops aws "configure list"
./ops bash -c "sam --version"
```